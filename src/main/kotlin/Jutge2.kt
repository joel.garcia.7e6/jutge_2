import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File
import java.util.*
import kotlinx.serialization.decodeFromString
import kotlin.math.round



const val RESET = "\u001b[0m"
const val BOX = "\u001b[51m"
const val BOLD = "\u001b[1m"
const val UNDERLINE = "\u001b[21m"
const val RED = "\u001b[31m"
const val CYAN = "\u001b[38;5;87m"
const val GREEN = "\u001b[38;5;10m"
const val ORANGE = "\u001b[38;5;202m"
const val YELLOW = "\u001b[38;5;11m"
const val GRAY = "\u001B[7m"
const val PINK = "\u001b[38;5;177m"
const val PURPLE = "\u001b[38;5;99m"
const val BLUE = "\u001b[38;5;69m"

val sc = Scanner(System.`in`)
fun main() {
    var numProblema: Int
    val listaProblemas =  File("src/main/resources/problemas.json").readLines()
    val listaIntentos =  File("src/main/resources/intentos.json").readLines()
    var currentProblema: Problema
    var problema: Problema
    var instruction: Int
    do{
        showMenu("Main")
        instruction = sc.nextLine().toInt()
        when(instruction){
            1 ->{
                var stop = false
                val problemasRemaining = itinerariAprenentatge(listaIntentos, listaProblemas)
                while (!stop) {
                    for (i in problemasRemaining){
                        numProblema = i-1
                        println("$GRAY Problema  ${numProblema+1} $RESET")

                        problema = Json.decodeFromString(listaProblemas[numProblema])
                        currentProblema = Problema(problema.numProblema, problema.enunciado,problema.inputPub,
                            problema.outputPub,problema.inputPriv,problema.outputPriv)

                        currentProblema.mostrarProblema(problema)

                        println("Vols intentar aquest problema?\n" +
                                "1-SI\n" +
                                "2-SEGUENT\n" +
                                "3-TORNAR LA MENU")

                        val intentar = sc.nextLine().uppercase()
                        if (intentar == "1"){
                            val intentoProblema = currentProblema.intentarProblema(problema.numProblema, problema)
                            if (intentoProblema) {
                                println("Has acertat !")
                            } else{
                                println("No has pogut amb el problema")
                            }
                        } else if (intentar == "3"){
                            stop = true
                            break
                        }
                    }
                }

            }
            2 ->{
                val problemAIntentar = showProblemList(listaProblemas)
                if (problemAIntentar != 0){
                    currentProblema = decodeProblem(problemAIntentar)
                    println("Problema $BOX$BOLD$PINK $problemAIntentar $RESET")
                    currentProblema.mostrarProblema(currentProblema)
                    val intentoProblema = currentProblema.intentarProblema(problemAIntentar, currentProblema)
                    if (intentoProblema) {
                        println("CORRECTE")
                    } else{
                        println("No has pogut amb el problema")
                    }
                }

            }
            3 -> showHistoryOfCompletedProblems(listaIntentos)
            4 ->{
                if (teacherLogin()){
                    do {
                        showMenu("Teacher")
                        val teacherInstruction = sc.nextLine().toInt()
                        when (teacherInstruction){
                            1 ->{
                                do {
                                    addNewProblem(listaProblemas)
                                    println("Problema afegit, vols afegir un altre?\n$GREEN$BOLD$BOX SI $RESET $RED$BOLD$BOX NO $RESET")
                                    val altreProblema = sc.nextLine().uppercase()
                                } while (altreProblema != "NO")
                            }
                            2 -> getPuntuation(getIntents(listaIntentos), listaProblemas)
                        }
                    } while (teacherInstruction != 0)
                } else{
                    println("Tornant al mode ALUMNE")
                }
            }
            5 -> println("TANCANT...")
        }

    } while(instruction != 6)
}
fun decodeProblem(numProblema: Int): Problema {
    val listaProblemas = File("src/main/resources/problemas.json").readLines()
    val problema: Problema = Json.decodeFromString(listaProblemas[numProblema-1])
    return Problema(
        problema.numProblema, problema.enunciado, problema.inputPub,
        problema.outputPub, problema.inputPriv, problema.outputPriv,
    )
}
fun showProblemList(listaProblemas: List<String>): Int {
    var problemAIntentar: String
    var numProblema = 0
    var currentProblema: Problema
    var problema: Problema
    for (i in listaProblemas){
        println("$GRAY Problema  ${numProblema+1} $RESET")

        problema = Json.decodeFromString(listaProblemas[numProblema])
        currentProblema = Problema(problema.numProblema, problema.enunciado,problema.inputPub,
            problema.outputPub,problema.inputPriv,problema.outputPriv)

        currentProblema.mostrarProblema(problema)
        numProblema++
    }
    println("""Entra el$BOLD número$RESET del problema que vols intentar
        |Si no vols intentar cap problema entra $BOLD$ORANGE 0 $RESET
    """.trimMargin())
    do {
        problemAIntentar = sc.nextLine()
    } while (problemAIntentar.toInt() > listaProblemas.size)
    return problemAIntentar.toInt()
}

fun addNewProblem(listaProblemas: List<String>){
    val nextProblemNumber = listaProblemas.size+1
    println("Entra el enunciat")
    val enunciado = sc.nextLine()
    println("""Entra les entrades del joc de proves public
        |Primer entra l'explicació de la entrada
        |Després entra les entrades
        |Entra $BLUE$BOLD!$RESET quan acabis per desar.
    """.trimMargin())
    val inputPublic = mutableListOf<String>()
    do {
        val singleInput = sc.nextLine()
        if (singleInput != "!"){
            inputPublic.add(singleInput)
        }
    } while (singleInput != "!")
    println("""Entra les sortides del joc de proves public
        |Primer entra l'explicació de la sortida
        |Després entra les sortides
        |Entra $BLUE$BOLD!$RESET quan acabis per desar.
    """.trimMargin())
    val outputPublic = mutableListOf<String>()
    do {
        val singleOutput = sc.nextLine()
        if (singleOutput != "!"){
            outputPublic.add(singleOutput)
        }
    } while (singleOutput != "!")

    println("""Entra les entrades del joc de proves privat
        |Entra tantes entrades com vulguis
        |Entra $BLUE$BOLD!$RESET quan acabis per desar.
    """.trimMargin())

    val inputPriv = mutableListOf<String>()
    do {
        val singleInput = sc.nextLine()
        if (singleInput != "!"){
            inputPriv.add(singleInput)
        }
    } while (singleInput != "!")

    println("""Entra les sortides del joc de proves privat
        |Entra tantes sortides com vulguis
        |Entra $BLUE$BOLD!$RESET quan acabis per desar.
    """.trimMargin())
    val outputPrivat = mutableListOf<String>()
    do {
        val singleOutput = sc.nextLine()
        if (singleOutput != "!"){
            outputPrivat.add(singleOutput)
        }
    } while (singleOutput != "!")

    val newProblema = Problema(nextProblemNumber, enunciado,inputPublic.toTypedArray(),
        outputPublic.toTypedArray(),inputPriv.toTypedArray(),outputPrivat.toTypedArray())

    val encodeProblem = Json.encodeToString(newProblema )
    File("src/main/kotlin/problemes/problemes.json").appendText("\n"+encodeProblem)

}
fun teacherLogin(): Boolean {
    var password: String
    var intents = 3
    var profeName: String
    println("Entra el teu nom de profe")
    do{
        profeName = sc.nextLine().uppercase()
        if (profeName != "JORDI" && profeName != "DANI" && profeName != "JOSELUIS"){
            println("Aquest profe no existeix")
        }
    } while (profeName != "JORDI" && profeName != "DANI" && profeName != "CIDO")
    println("Benvingut $profeName")
    println("Entra la contrasenya")
    do {
        password = sc.nextLine()
        if (password != "profeITB"){
            intents--
            println("Contrasenya incorrecta, et queden $RED $intents $RESET intents")
        }
    } while (password != "profeITB" && intents != 0)

    return intents != 0
}

fun showHistoryOfCompletedProblems(listaIntentos: List<String>) {
    for (i in listaIntentos) {
        val intento = Json.decodeFromString<Intento>(i)
        println("$GRAY Problema ${intento.numProblema} $RESET")
        println(intento.enunciado)
        println("$PINK Entrada:$RESET ${intento.inputPriv}")
        println("$PINK Sortida:$RESET ${intento.outputPriv}")
        if (intento.resuelto){
            println("$PINK Resolt: $GREEN SI $RESET")
        } else println("$PINK Resolt: $RED NO $RESET")
        println("$PINK Intents:$RESET ${intento.intentos}")
    }
}
fun itinerariAprenentatge(listaIntentos: List<String>, listaProblemas: List<String>): MutableList<Int> {
    val listaNumeros = mutableListOf<Int>()
    for (i in listaProblemas) {
        val problema = Json.decodeFromString<Problema>(i)
        listaNumeros.add(problema.numProblema)
    }
    for (i in listaIntentos) {
        val intento = Json.decodeFromString<Intento>(i)
        if (intento.resuelto) {
            listaNumeros.remove(intento.numProblema)
        }
    }
    return listaNumeros
}
fun showMenu(menuType: String){
    when (menuType){
        "Main" -> {
            println("1- Seguir amb l'itinerari del projecte")
            println("2- Llista de problemes")
            println("3- Consultar històric de problemes resolts")
            println("4- Identifiació de professorat")
            println("5- Sortir")
        }
        "Teacher" -> {
            println("1- Afegir nous problemes")
            println("2- Treure report de la feina")
            println("0- Enrere")
        }
    }

}
fun getPuntuation(problemasResueltos: List<Pair<Int,Int>>, listaProblemas: List<String>) {
    val problemasTotales = listaProblemas.size
    println("S'han resolt  ${problemasResueltos.size}  de $problemasTotales ")
    println()
}

fun getIntents(listaIntentos: List<String>): List<Pair<Int, Int>> {
    val problemasResueltos = mutableSetOf<Int>()
    val problemasResueltosWithIntentos = mutableSetOf<Pair<Int, Int>>()
    for (i in listaIntentos) {
        val intento = Json.decodeFromString<Intento>(i)
        if (intento.resuelto) {
            if (intento.numProblema !in problemasResueltos) {
                problemasResueltos.add(intento.numProblema)
                problemasResueltosWithIntentos.add(intento.numProblema to intento.intentos)
            }
        }
    }
    return problemasResueltosWithIntentos.sortedBy { it.first }
}
fun Double.round(decimals: Int): Double {
    var multiplier = 1.0
    repeat(decimals) {
        multiplier *= 10
    }
    return round(this * multiplier) / multiplier
}

@Serializable
class Problema (var numProblema: Int, var enunciado: String, var inputPub: Array<String>,
                var outputPub: Array<String>, var inputPriv: Array<String>,
                var outputPriv: Array<String>) {

    fun mostrarProblema(problema: Problema){
        println(problema.enunciado)
        println("$CYAN Exemple 1")
        println("${problema.inputPub[0]}\nEntrada: ${problema.inputPub[1]}")
        println("${problema.outputPub[0]}\nSortida: ${problema.outputPub[1]}$RESET")
        println("$PURPLE Exemple 2")
        println("Entrada: ${problema.inputPub[2]}")
        println("Sortida: ${problema.outputPub[2]}$RESET")
    }

    fun intentarProblema(numProblema: Int, currentProblema: Problema): Boolean {
        val random = (0..2).random()
        println("$PURPLE Entrada:$RESET ${currentProblema.inputPriv[random]}")
        println("Per abandonar el problema entra $ORANGE 0 $RESET")
        print("$PURPLE Sortida:$RESET ")
        var userAnswer: String
        var resolt = false
        var intents = 0
        val listOfUserAnswers = mutableListOf<String>()
        do {
            intents++
            userAnswer = sc.nextLine().uppercase()
            if (userAnswer != currentProblema.outputPriv[random].uppercase() && userAnswer != "0" ) {
                println("Resposta incorrecta, torna-ho a intentar")
            } else if (userAnswer == currentProblema.outputPriv[random].uppercase()){
                resolt = true
            }
            listOfUserAnswers.add(userAnswer)

        } while (userAnswer != currentProblema.outputPriv[random].uppercase() && userAnswer != "0")

        val userIntent = Json.encodeToString(Intento(numProblema, currentProblema.enunciado,
            currentProblema.inputPriv[random], listOfUserAnswers, intents, resolt ))

        File("src/main/resources/intentos.json").appendText(userIntent+"\n")

        return resolt

    }

}

@Serializable
data class Intento(val numProblema: Int, val enunciado: String, val inputPriv: String,
                   val outputPriv: MutableList<String>, val intentos: Int, val resuelto: Boolean)

